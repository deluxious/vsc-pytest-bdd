# Pytest BDD Support

Do [Python BDD](https://github.com/pytest-dev/pytest-bdd) things in VSC.


## Features

* Exists


## Requirements

* You do [python](https://www.python.org/) things.
* You do [pytest](https://pytest.org/) things.
* You do [pytest-bdd](https://pytest-bdd.readthedocs.io/en/latest/) things.
* You do these things with [vsc](https://code.visualstudio.com/).


## Known Issues

* Has no functionality. (Major issue.)


## Release Notes

* Nothing yet.
